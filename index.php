<?php
header('Content-type: text/html; charset=utf-8');

$pages = array(
    'o-nas' => array(
        'tmpl' => 'about',
        'title' => 'O nas'
    ),
    'oferta' => array(
        'tmpl' => 'offer',
        'title' => 'Oferta'
    ),
    'porady' => array(
        'tmpl' => 'counsels',
        'title' => 'Porady'
    ),
    'kontakt' => array(
        'tmpl' => 'contact',
        'title' => 'Kontakt'
    ),
    'kancelaria-doradztwa-podatkowego' => array(
        'tmpl' => 'taxing',
        'title' => 'Kancelaria Doradztwa Podatkowego'
    ),
    'biuro-rachunkowe' => array(
        'tmpl' => 'accounting',
        'title' => 'Biuro Rachunkowe'
    ),
);
$root = '/saldo';


if (isset($_GET['page'])) {
    if (array_key_exists($_GET['page'], $pages)) {
        include_once 'header.php';
        include_once 'content/' . $pages[$_GET['page']]['tmpl'] . '.php';
        include_once 'footer.php';
    } else {
        header('Location: ' . $root);
    }
} else {
    include_once 'header.php';
    include_once 'content/main.php';
    include_once 'footer.php';
}

function get_title()
{
    global $pages;
    if (isset($_GET['page']))
        return $pages[$_GET['page']]['title'];
    else
        return 'Strona Główna';
}

function get_page_style()
{
    global $pages;
    if (isset($_GET['page']))
        return $pages[$_GET['page']]['tmpl'] . '.min.css';
    else
        return 'main.min.css';
}