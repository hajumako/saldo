<section class="page-title-wrapper">
    <div class="wrapper">
        <h1>O Firmie</h1>
        <p>Świadczymy usługi księgowe nieprzerwanie od ponad 25 lat.<br>Aktualnie obsługujemy około 200 firm. 80% klientów współpracuje z nami od ponad 10 lat.</p>
    </div>
</section>
<section class="subtitle-wrapper">
    <div class="wrapper">
        <p>
            Świadczymy wysoką jakość usług księgowo-podatkowych dla Twojej firmy. Zapewniamy bezpieczeństwo informacji, profesjonalny zespół ekspertów i indywidualne podejście.
            Współpracując na bieżąco z klientem rozpoznajemy jego aktualne potrzeby w zakresie księgowości i podatków. Dzięki takiemu podejściu, możemy dopasować się do tempa
            rozwoju naszych klientów i zmieniającego się rynku.
        </p>
    </div>
</section>
<section class="team">
    <div class="wrapper">
        <ul>
            <li>
                <img src="img/anna_chojnacka.jpg" alt="Anna Chojnacka"/>
                <h6>Anna Chojnacka</h6>
                <p>Specjalista ds. rachunkowści</p>
                <a href="tel:+48606475745">+48 606 475 745</a>
            </li>
            <li>
                <img src="img/wieslaw_chojnacki.jpg" alt="Wiesław Chojnacki"/>
                <h6>Wiesław Chojnacki</h6>
                <p>Doradca Podatkowy Nr 03808</p>
                <a href="tel:. +48602645058">+48 602 645 058</a>
            </li>
            <li>
                <img src="img/bartosz_chojnacki.jpg" alt="Bartosz Chojnacki"/>
                <h6>Bartosz Chojnacki</h6>
                <p>Specjalista ds. rachunkowości</p>
                <a href="tel:+48 602649058">+48 602 649 058</a>
            </li>
        </ul>
    </div>
</section>
<section class="history">
    <div class="wrapper">
        <h3>Historia</h3>
        <ul>
            <li class="odd">
                <h4>2016</h4>
                <p>Kancelaria Saldo Chojnaccy przeniosła się do nowej, reprezentacyjnej siedziby.</p>
            </li>
            <li>
                <p>Powstała nowa Kancelaria Doradztwa Podatkowego pod nazwą Saldo Chojnaccy.</p>
                <h4>2016</h4>
            </li>
            <li class="odd">
                <h4>2004</h4>
                <p>Wdrożony został system informatyczny DatevSymfonia.</p>
            </li>
            <li>
                <p>Wiesław Chojnacki został wybrany na funkcję Sekretarza i członka Prezydium Zarządu Pomorskiego Oddziału Krajowej Izby Doradców Podatkowych.</p>
                <h4>2002</h4>
            </li>
            <li class="odd">
                <h4>2001</h4>

                <p>Wiesław Chojnacki został wybrany delegatem z województwa pomorskiego na Pierwszy Zjazd Krajowej Rady Doradców Podatkowych.</p>
            </li>
            <li>
                <p>Po ustawowym uregulowaniu zawodu doradcy podatkowego, wspólnicy spółki uzyskali wpisy na listę osób wykonujących ten zawód.</p>
                <h4>1998</h4>
            </li>
            <li class="odd">
                <h4>1991</h4>
                <p>Podpisano pierwszą umowę dotyczącą świadczenia usług księgowych i doradztwa podatkowego.</p>
            </li>
            <li>
                <p>Powstała Spółka cywilna Saldo.</p>
                <h4>1991</h4>
            </li>
        </ul>
    </div>
</section>