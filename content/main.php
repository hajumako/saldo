<section class="heading-wrapper">
    <div id="left" class="heading">
        <div class="wrapper">
            <h2>Kancelaria Doradztwa Podatkowego</h2>
            <ul>
                <li>Doradztwo podatkowe,</li>
                <li>reprezentowanie klientów przed urzędami,</li>
                <li>reprezentowanie przed sądami w postępowaniu administracyjnym,</li>
                <li>sporządzanie deklaracji podatkowych i wniosków kredytowych,</li>
                <li>sporządzenie analiz podatkowych oraz prognoz,</li>
                <li>sporządzanie opinii podatkowych,</li>
                <li>wprowadzenie jednolitego pliku kontrolnego (JPK).</li>
            </ul>
            <a class="button" href="kancelaria-doradztwa-podatkowego">Więcej</a>
        </div>
    </div>
    <div id="right" class="heading">
        <div class="wrapper">
            <h2>Biuro Rachunkowe</h2>
            <ul>
                <li>Księgi rachunkowe,</li>
                <li>podatkowe księgi przychodów i rozchodów,</li>
                <li>rozliczenie ryczałtu ewidencjonowanego,</li>
                <li>kadry i płace,</li>
                <li>sprawozdania i raporty finansowe.</li>
            </ul>
            <a class="button" href="biuro-rachunkowe">Więcej</a>
        </div>
    </div>
</section>

<section class="values">
    <div class="wrapper">
        <ul>
            <li id="column">
                <h3>25&nbsp;lat&nbsp;historii<br>firmy</h3>
                <p>Jesteśmy jedną z pierwszych kancelarii podatkowych w Gdańsku.</p>
            </li>
            <li id="tree">
                <h3>Firma<br>Rodzinna</h3>
                <p>Jesteśmy firmą rodzinną. Kapitał&nbsp;budowany latami pracuje na Państwa korzyść.</p>
            </li>
            <li id="trophy">
                <h3>Ponad&nbsp;200&nbsp;firm<br>i instytucji</h3>
                <p>Współpracujemy z ponad dwustu podmiotami zarówno polskimi jak i zagranicznymi.</p>
            </li>
        </ul>
    </div>
</section>

