<section class="page-title-wrapper">
    <div class="wrapper">
        <h1>Porady</h1>
        <p>Z chęcią udzielimy odpowiedzi na nurtujące Cię pytania.<br>Nie znalazłeś odpowiedzi? Skontaktuj się z nami.</p>
    </div>
</section>
<section class="counsels">
    <div class="wrapper">
        <div class="problems">
            <div class="wrapper">
                <h2>Problemy</h2>
                <ul>
                    <li>
                        <a href="#p1">Kiedy odliczyć VAT jeżeli data sprzedaży różni się od daty zapłaty?</a>
                    </li>
                    <li>
                        <a href="#p2">Optymalizacja podatkowa - kiedy jest legalna?</a>
                    </li>
                    <li>
                        <a href="#p3">Rozliczenie WNT</a>
                    </li>
                    <li>
                        <a href="#p4">Przekroczony próg podatkowy</a>
                    </li>
                    <li>
                        <a href="#p5">Nowe zasady kontroli PIP w firmach</a>
                    </li>
                    <li>
                        <a href="#p7">Darowizna 1% podatku</a>
                    </li>
                    <li>
                        <a href="#p8">Opodatkowanie sprzedaży nieruchomości</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="solutions">
            <div class="wrapper">
                <ul>
                    <li id="p1">
                        <h2>Kiedy odliczyć VAT jeżeli data sprzedaży różni się od daty zapłaty?</h2>
                        <p>Przy rozliczaniu podatku VAT od zakupów należy zwrócić szczególną uwagę na datę otrzymania dokumentu. Faktury wystawiane pod koniec miesiąca często
                            wpływają
                            dopiero w miesiącu następnym. Jeśli podatnik za wcześnie rozliczy VAT z nich wynikający biorąc pod uwagę datę wystawienia, to naraża się na
                            konsekwencje. W
                            razie wykrycia przez urząd skarbowy błędnego okresu odliczenia VAT - będzie musiał dopłacić stosowne odsetki. Podatek VAT można bowiem odliczyć w
                            momencie
                            otrzymania faktury. Należy jednak mieć na względzie aby pomiędzy datą wystawienia a otrzymania dokumentu nie było zbyt dużej rozbieżności, np. data
                            wystawienia
                            - luty a otrzymania - czerwiec. Warto mieć wtedy jakiś dowód, że dokument faktycznie wpłynął do firmy z opóźnieniem. Może nim być np. dołączona koperta,
                            na
                            której będzie się znajdować pieczątka z datą nadania.</p>
                    </li>
                    <li id="p2">
                        <h2>Optymalizacja podatkowa - kiedy jest legalna?</h2>
                        <p>
                            Podatnicy coraz częściej korzystają z różnych możliwości legalnego obniżenia obciążeń podatkowych. Popularne jest stosowanie struktur optymalizacyjnych,
                            polegających na podejmowaniu zgodnych z prawem działań, których rezultatem jest uniknięcie powstania zobowiązania podatkowego, które w przeciwnym
                            wypadku by
                            powstało. Optymalizacja podatkowa oparta jest, z założenia, na działaniach zgodnych z prawem, nie może być więc skutecznie kwestionowana przez organy
                            podatkowe.
                        </p>
                        <p>
                            Działania takie powodują jednak spadek wpływów do budżetu państwa, stąd Ministerstwo Finansów planuje wprowadzić klauzulę zabraniającą obejścia prawa
                            podatkowego.
                        </p>
                        <p>
                            Prześledźmy jak wygląda obecnie zakres dozwolonej optymalizacji podatkowej i jak sytuacja zmieni się po ewentualnym wprowadzeniu takiej klauzuli.
                        </p>
                        <h3>Przykład</h3>
                        <p>Podatnik dokonuje darowizny na rzecz osoby z najbliższej rodziny. Obdarowany także dokonuje darowizny na rzecz osoby ze swojej najbliższej rodziny. Obie
                            zawarte
                            darowizny zostały dokonane między osobami z najbliższej rodziny, mogą więc korzystać ze zwolnienia z podatku od spadków i darowizn.
                        </p>
                        <h3>WAŻNE</h3>
                        <p>
                            Od optymalizacji podatkowej, która jest co do zasady dozwolona, należy odróżnić sytuację, w której ma miejsce czynność pozorna, ponieważ taka transakcja
                            może
                            być
                            już obecnie zakwestionowana przez organy podatkowe. Typowym przykładem czynności pozornej jest zawarcie dwóch umów darowizny zamiast umowy zamiany. W
                            tym
                            przypadku, organy podatkowe, zgodnie z obecnie obowiązującymi przepisami, mogą wykazać, że strony umów darowizny chciały wywołać skutki właściwe dla
                            umowy
                            zamiany i określić konsekwencje podatkowe właściwe dla tej umowy. W niektórych sytuacjach określenie czy mamy do czynienia z optymalizacją podatkową czy
                            z
                            czynnością pozorną może być trudne.
                            Po wprowadzeniu planowanej regulacji, organy podatkowe będą mogły zakwestionować konsekwencje podatkowe rozpoznane przez podatnika, jeżeli ciąg
                            dokonanych
                            transakcji będzie mieć charakter sztuczny, a ten sam rezultat można by było osiągnąć w bardziej adekwatny sposób, a ponadto dominującą korzyścią
                            wynikającą
                            z
                            wyboru bardziej skomplikowanej struktury była korzyść podatkowa. W takiej sytuacji zostanie naliczony podatek jaki byłby należny, w przypadku
                            zastosowania
                            struktury bardziej adekwatnej dla danej sytuacji.
                            Podatnik zostanie także obciążony dodatkową sankcją, wynoszącą 30 proc. uszczupleń należnych zobowiązań podatkowych. Organy podatkowe uzyskają więc
                            skuteczne
                            narzędzie do walki z optymalizacją podatkową. Jednak właściwa ocena czy zostały spełnione kryteria zastosowania klauzuli obejścia prawa może być czasem
                            bardzo
                            trudna, a odpowiednie zaplanowanie transakcji nabierze kluczowego znaczenia. Poniższe przykłady pokazują jak szczegółowe warunki transakcji mogą
                            zmieniać
                            jej
                            ocenę z punktu widzenia obejścia prawa.
                        </p>
                        <h3>Przykład 2</h3>
                        <p> Sytuacja jak w Przykładzie 1, z tym że w przypadku pierwszej darowizny chodzi o prezent urodzinowy. Ponieważ prezent urodzinowy okazał się nietrafiony,
                            obdarowany postanowił oddać go swojemu dziecku, któremu się on podobał.
                        </p>
                        <p>
                            W tym przypadku raczej nie doszło do obejścia prawa podatkowego, ponieważ istnieją, inne niż korzyść podatkowa, okoliczności uzasadniające dokonanie
                            każdej
                            z
                            darowizn.
                        </p>
                        <h3>Przykład 3</h3>
                        <p>
                            Sytuacja będzie inna, jeżeli pierwsza darowizna dokonana jest na dzień przed pierwszą komunią i chodzi o dziecięcy rower. Obdarowany tego samego dnia
                            dokonuje
                            darowizny na rzecz dziecka przystępującego do pierwszej komunii i chodzi o ten sam rower.
                        </p>
                        <p>
                            Tym razem dokonane czynności miały charakter sztuczny i organy podatkowe mogłyby zastosować klauzulę obejścia prawa.
                        </p>
                    </li>
                    <li id="p3">
                        <h2>Rozliczenie WNT</h2>
                        <p>
                            Przedsiębiorca, który sprowadza towary z innego kraju Wspólnoty, ma wystawić fakturę wewnętrzną i wykazać podatek od wewnątrzwspólnotowego nabycia. Musi
                            tak
                            postąpić, nawet gdy dysponuje tylko wydrukiem z kasy fiskalnej.
                        </p>
                        <p>
                            Przedsiębiorcy, którzy kupują towary za granicą, mogą mieć problem z uzyskaniem faktury. Przy drobnych transakcjach często jedynym dokumentem, na jaki
                            mogą
                            liczyć, jest paragon z kasy fiskalnej. Zagraniczni sprzedawcy argumentują, że można nim dokumentować sprzedaż dla małych przedsiębiorstw lub sprzedaż o
                            małej
                            wartości. Warto więc pamiętać, że udokumentowanie transakcji paragonem nie zwalnia nabywcy z obowiązku rozliczenia wewnątrzwspólnotowego nabycia towarów
                            (WNT).
                        </p>
                        <p>
                            Kiedy wystawiamy fakturę wewnętrzną
                        </p>
                        <p>
                            WNT podlega opodatkowaniu polskim VAT. Z transakcją taką mamy do czynienia, tylko jeżeli łącznie spełnione są następujące warunki:
                        </p>
                        <ul>
                            <li>dostawa towarów odbywa się za wynagrodzeniem,</li>
                            <li>następuje przeniesienie na nabywcę prawa do rozporządzania towarem jak właściciel,</li>
                            <li>towary w wyniku dostawy są wysłane lub transportowane do Polski z innego państwa członkowskiego,</li>
                            <li>wysyłka bądź transport towarów odbywa się przez dostawcę, nabywcę lub na ich rzecz.</li>
                        </ul>
                        <p>
                            Dodatkowym warunkiem rozliczenia WNT jest status dostawcy i nabywcy. Dostawca towarów musi być podatnikiem podatku od wartości dodanej. Nabywca zaś musi
                            być
                            podatnikiem polskiego VAT, a nabywane przez niego towary muszą służyć działalności gospodarczej.
                        </p>
                        <p>Tylko na cele firmy</p>
                        <p>
                            Ustawa o VAT nie reguluje, w jaki sposób dostawa towarów powinna być dokumentowana przez unijnego sprzedawcę. Do WNT więc dojdzie, nawet jeżeli dostawca
                            nie
                            wystawi żadnego dokumentu sprzedaży. Jeżeli jednak wystawi fakturę dokumentującą sprzedaż towaru, to może ona przesądzać o dacie powstania obowiązku
                            podatkowego
                            w WNT.
                        </p>
                        <p>
                            Co jednak, gdy unijny sprzedawca udokumentuje transakcję paragonem fiskalnym? Wtedy też trzeba rozliczyć WNT. Oczywiście pod warunkiem, że kontrahenci
                            są
                            podatnikami VAT, a towar w wyniku sprzedaży został przywieziony z innego państwa członkowskiego do Polski.
                        </p>
                        <p>
                            Warunkiem opodatkowania transakcji jest przywiezienie towaru do Polski dla celów prowadzonej przez polskiego nabywcę działalności gospodarczej. WNT nie
                            wystąpi
                            więc, jeżeli przedsiębiorca w trakcie wakacji kupi za granicą dla swoich prywatnych potrzeb np. aparat fotograficzny, który po zakończonym urlopie
                            przywiezie do
                            kraju.
                        </p>
                    </li>
                    <li id="p4">
                        <h2>Przekroczony próg podatkowy</h2>
                        <p>
                            Pod koniec roku warto sprawdzić, czy wynagrodzenia pracowników nie przekroczyły progów podatkowych lub ZUS. Prawidłowe rozliczenie pozwoli uniknąć
                            korekt i
                            zapewni porządek w dokumentacji. Problem wydaje się trudny, bo wysokości ograniczeń dla zaliczki i składek są różne. Zasady obliczania również. Jak
                            zatem
                            postępować w praktyce? Graniczna kwota dotyczy składek emerytalnej i rentowej
                        </p>
                        <p>
                            Wynagrodzenie pracowników podlega obowiązkowo ubezpieczeniom emerytalnemu i rentowemu. Co prawda nie są to jedyne ubezpieczenia, które należy
                            uwzględniać
                            przy
                            obliczaniu pensji, jednak tylko dla nich wprowadzono górną roczną granicę podstawy wymiaru. Na podstawie art. 19 ust. 1 ustawy z 13 października 1998 r.
                            o
                            systemie ubezpieczeń społecznych (Dz.U. z 2009 r. Nr 205, poz. 1585 ze zm.) roczne ograniczenie składek na ubezpieczenia emerytalne i rentowe w roku
                            kalendarzowym odpowiada 30-krotności prognozowanego przeciętnego wynagrodzenia miesięcznego w gospodarce narodowej na dany rok kalendarzowy. W 2013 r.
                            kwota
                            limitu wynosi 111 390 zł (obwieszczenie Ministra Pracy i Polityki Społecznej z 14.12.2012 r. w sprawie kwoty ograniczenia rocznej podstawy wymiaru
                            składek
                            na
                            ubezpieczenia emerytalne i rentowe w roku 2013 oraz przyjętej do jej ustalenia kwoty prognozowanego przeciętnego wynagrodzenia - M.P. 2012 poz. 1018).
                        </p>
                        <p>
                            Niezależnie od przekroczenia granicznej kwoty składek emerytalno-rentowych, od pełnej kwoty wynagrodzenia należy obliczać i odprowadzać pozostałe
                            składki na
                            ubezpieczenie chorobowe, wypadkowe, zdrowotne, a także na Fundusz Pracy i Fundusz Gwarantowanych Świadczeń Pracowniczych.
                        </p>
                    </li>
                    <li id="p5">
                        <h2>Nowe zasady kontroli PIP w firmach</h2>
                        <p>
                            Z dniem 8 sierpnia 2011 roku zaczęła obowiązywać nowelizacja ustawy z dnia 13 kwietnia 2007 r. o Państwowej Inspekcji Pracy (Dz.U. z 2007 r. Nr 89, poz.
                            589
                            ze
                            zm.) dalej jako ustawa o PIP, która zmienia zasady kontroli Państwowej Inspekcji Pracy w firmach.
                        </p>
                        <p>
                            Nowe przepisy pozwalają inspektorom pracy dodatkowo uzyskiwać dostęp do baz zgromadzonych w rejestrze bezrobotnych, w Krajowym Rejestrze Sądowym,
                            Krajowym
                            Rejestrze Karnym oraz ZUS.
                        </p>
                        <p>
                            Nowelizacja rozszerzyła także krąg osób, które mogą towarzyszyć inspektorowi pracy podczas kontroli. Zgodnie z art. 22 ust. 3 ustawy o PIP,
                            współuczestnikami
                            kontroli mogą być wszyscy pracownicy PIP, mający niezbędną wiedzę o kontroli, a działać będą na podstawie imiennego upoważnienia wydanego przez Głównego
                            lub
                            Okręgowego Inspektora Pracy.
                        </p>
                        <p>
                            Duża część zmian w ustawie wprowadza formy prewencyjnego oddziaływania na pracodawców i przedsiębiorców. Z działalnością prewencyjną wiąże się także
                            modyfikacja
                            pozwalająca traktować pierwszą kontrolę jako audyt, a nie typową kontrolę i zakończyć ją bez karania mandatem za popełnione wykroczenia. Dotyczy to
                            pracodawców
                            rozpoczynających dopiero działalność, u których w trakcie kontroli nie stwierdzono wykroczeń z winy umyślnej, a także nie stwierdzono bezpośredniego
                            zagrożenia
                            życia lub zdrowia pracowników.
                        </p>
                        <p>
                            Nowelizacja wprowadza, nową formę zaleceń czyli pouczenie ustne usunięcia uchybień, które można usunąć podczas trwania kontroli lub niezwłocznie po jej
                            zakończeniu. Fakt wydania pouczenia, ustnej decyzji należy jednak odnotować w protokole pokontrolnym. Pracodawca będzie zobowiązany poinformować PIP o
                            realizacji ustnego zalecenia - decyzji.
                        </p>
                        <p>
                            Nowe przepisy mogą w znaczny sposób uprościć kontrole, a także być skuteczne wobec łamania praw pracowniczych, acz mogą też spowodować wzrost sporów
                            pomiędzy
                            Inspekcją a pracodawcami.
                        </p>
                    </li>
                    <li id="p7">
                        <h2>Darowizna 1% podatku</h2>
                        <p>Podatnicy rozliczający się za 2012 rok mają możliwość wskazania organizacji pożytku publicznego (OPP), którą chcą obdarować 1% swojego podatku należnego.
                            Darowiznę taką może przekazać podatnik podatku dochodowego od osób fizycznych, podatnik opodatkowany ryczałtem od przychodów ewidencjonowanych, podatnik
                            objęty
                            liniową 19% stawką podatku, podatnik uzyskujący dochód z odpłatnego zbycia papierów wartościowych oraz pochodnych instrumentów finansowych, podatnik
                            uzyskujący
                            dochód ze sprzedaży nieruchomości oraz podatnik podatku dochodowego od osób prawnych. Na rzecz OPP 1% podatku mogą złożyć także emeryci i renciści,
                            którzy
                            otrzymali od organu rentowego PIT-40 A.
                        </p>
                        <p>
                            Jeżeli chodzi o działalność pożytku publicznego, kwestia ta uregulowana została w ustawie o działalności pożytku publicznego i wolontariacie. Przepis
                            art. 3
                            tej
                            ustawy stanowi, iż działalnością pożytku publicznego jest działalność społecznie użyteczna, prowadzona przez organizacje pozarządowe w sferze zadań
                            publicznych.
                            Sfera zadań publicznych obejmuje takie obszary m.in. jak pomoc społeczna, niepełnosprawni, czy działalność charytatywna.
                        </p>
                        <p>
                            W celu przekazania 1% podatku na rzecz organizacji pożytku publicznego należy wypełnić odpowiednie rubryki w rocznym zeznaniu podatkowym PIT-28, PIT-36,
                            PIT-
                            36L, PIT-37, PIT-38 lub PIT–39 za rok 2012 zatytułowaną „Wniosek o przekazanie 1% podatku należnego na rzecz organizacji pożytku publicznego”.
                        </p>
                        <p>
                            Aby organizacja pożytku publicznego otrzymała nasz podatek konieczne jest wpisanie tylko jej numeru KRS w zeznaniu podatkowym za rok 2012. Jest to duże
                            ułatwienie dla podatników w porównaniu do lat ubiegłych, ponieważ nie trzeba już wpisywać pełnej nazwy organizacji oraz numeru rachunku bankowego.
                            Wyboru
                            organizacji pożytku publicznego uprawnionych do otrzymania podatku możemy dokonać ze spisu prowadzonego przez Ministra Pracy i Polityki Społeczne na
                            stronie
                            www.mpips.gov.pl.
                        </p>
                    </li>
                    <li id="p8">
                        <h2>Opodatkowanie sprzedaży nieruchomości</h2>
                        <p>
                            Podatek dochodowy z tytułu sprzedaży nieruchomości podatnik zobowiązany jest zapłacić w przypadku:
                        </p>
                        <ul>
                            <li>uzyskania przychodu z odpłatnego zbycia nieruchomości lub ich części oraz udziału w nieruchomości,</li>
                            <li>spółdzielczego własnościowego prawa do lokalu mieszkalnego lub użytkowego oraz prawa do domu jednorodzinnego w spółdzielni mieszkaniowej,</li>
                            <li>prawa wieczystego użytkowania gruntów.</li>
                        </ul>
                        <p>
                            Jeżeli odpłatne zbycie nie następuje w wykonaniu działalności gospodarczej i zostało dokonane przed upływem 5 lat, licząc od końca roku kalendarzowego,
                            w
                            którym
                            nastąpiło nabycie lub wybudowanie.
                        </p>
                        <p>
                            Zasady rozliczenia, wysokość opodatkowania i możliwość skorzystania z ulg podatkowych przy odpłatnym zbyciu nieruchomości i praw majątkowych zależą od
                            daty
                            nabycia (wybudowania) zbywanej nieruchomości lub prawa majątkowego. To skutek dwóch zmian przepisów ustawy o podatku dochodowym od osób fizycznych.
                            Pierwsza
                            zmiana weszła w życie 1 stycznia 2007 r., druga obowiązuje od 1 stycznia 2009 r.
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>