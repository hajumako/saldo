<section class="page-title-wrapper">
    <div class="wrapper">
        <h1>Oferta</h1>
        <p>W ramach prowadzonej działalności proponujemy Państwu usługi doradztwa podatkowego oraz optymalizacji kosztów prowadzenia działalności gospodarczej Państwa firmy.</p>
        <p>Zachęcamy do zapoznania się ze szczegółowym zakresem usług wybierając odpowiednią kategorię:</p>
        <a class="button purple" href="kancelaria-doradztwa-podatkowego">Kancelaria Doradztwa Podatkowego</a>
        <a class="button orange" href="biuro-rachunkowe">Biuro Rachunkowe</a>
    </div>
</section>