<section class="page-title-wrapper">
    <div class="wrapper">
        <h1>Biuro Rachunkowe</h1>
        <p>Prowadzimy księgowość, obsługę kadrowo-płacową oraz pomagamy w prowadzeniu biznesu.<br>Oferujemy szeroki zakres usług świadczonych w najwyższym standardzie.</p>
    </div>
</section>
<section class="services">
    <div class="wrapper">
        <ul class="groups">
            <li id="i_1">
                <h2>Prowadzimy:</h2>
                <ul>
                    <li>księgi rachunkowe,</li>
                    <li>podatkowe księgi przychodów i rozchodów,</li>
                    <li>ewidencje przychodów dla zryczałtowanego podatku dochodowego,</li>
                    <li>ewidencje dla celów podatku VAT.</li>
                </ul>
            </li>
            <li id="i_2">
                <h2>Prowadzimy rozliczenia podatkowe w zakresie:</h2>
                <ul>
                    <li>podatku dochodowego osób fizycznych i prawnych,</li>
                    <li>podatku od towarów i usług,</li>
                    <li>podatku od czynności cywilno-prawnych,</li>
                    <li>podatku od spadków i darowizn,</li>
                    <li>podatku od nieruchomości.</li>
                   </ul>
                <p>Na podstawie ksiąg i rejestrów wypełniamy wszystkie niezbędne deklaracje podatkowe i sprawozdawcze.</p>
            </li>
            <li id="i_3">
                <h2>Pomagamy w obsłudze kadrowo-płacowej poprzez:</h2>
                <ul>
                    <li>reprezentowanie klientów przed Zakładem Ubezpieczeń Społecznych,</li>
                    <li>sporządzanie deklaracji dla Zakładu Ubezpieczeń Społecznych,</li>
                    <li>sprawowanie nadzoru nad prawidłowością dokumentacji kadrowej,</li>
                    <li>naliczanie wynagrodzeń pracowniczych,</li>
                    <li>sporządzanie deklaracji podatkowych PIT-4 i PIT-11,</li>
                    <li>dokonywanie rozliczeń z PFRON.</li>
                </ul>
            </li>
        </ul>
        <ul class="orphan">
            <li>
                <h2>Pomagamy w bieżącym prowadzeniu działalności gospodarczej:</h2>
                <ul>
                    <li>doradzamy w codziennej praktyce biznesowej,</li>
                    <li>przygotowujemy sprawozdania finansowe,</li>
                    <li>wypełniamy wnioski kredytowe,</li>
                    <li>sporządzamy sprawozdania dla GUS.</li>
                </ul>
            </li>
        </ul>
    </div>
</section>