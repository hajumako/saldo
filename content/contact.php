<section class="page-title-wrapper">
    <div class="wrapper">
        <h1>Kontakt</h1>
        <p>Zapraszamy do kontaktu z naszym zespołem, który chętnie odpowie na Twoje pytania.</p>
    </div>
</section>
<section class="contact-wrapper">
    <div class="wrapper">
        <ul>
            <li>
                <img src="img/magdalena_pietraszczyk.jpg" alt="Magdalena Pietraszczyk"/>
                <h6>Magdalena Pietraszczyk</h6>
                <p>Asystent Zarządu</p>
            </li>
            <li id="links">
                <a class="phone" href="tel:583440531">
                    <h5>Telefon</h5>
                    <span>(58) 344 05 31</span>
                </a>
                <a class="mail" href="mailto:biuro@chojnaccy.com">
                    <h5>Email</h5>
                    <span>biuro@chojnaccy.com</span>
                </a>
            </li>
            <li id="address">
                <p>Kancelaria Doradztwa Podatkowego Saldo<br>Wiesław Chojnacki</p>
                <p>ul. Chrzanowskiego 10/7<br>80-278 Gdańsk</p>
                <p>NIP: 584 181 70 50<br>REGON: 363665824</p>
            </li>
        </ul>
    </div>
</section>
<section class="map">
    <div id="map"></div>
</section>