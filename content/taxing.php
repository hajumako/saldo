<section class="page-title-wrapper">
    <div class="wrapper">
        <h1>Kancelaria Doradztwa Podatkowego</h1>
        <p>Proponujemy Państwu usługi doradztwa podatkowego<br>oraz optymalizacji kosztów prowadzenia działalności gospodarczej Państwa firmy.</p>
    </div>
</section>
<section class="services">
    <div class="wrapper">
        <ul class="groups">
            <li id="i_1">
                <h2>Rozpoczynając<br>współpracę:</h2>
                <ul>
                    <li>dokonujemy audytu podatkowego,</li>
                    <li>opracowujemy strategię optymalizacji podatkowej.</li>
                </ul>
            </li>
            <li id="i_2">
                <h2>W ramach<br>zawartych umów:</h2>
                <ul>
                    <li>doradzamy w zakresie codziennego stosowania prawa podatkowego,</li>
                    <li>obsługujemy klientów w Krajowym Rejestrze Sądowym oraz CEiDG,</li>
                    <li>sporządzamy wnioski o wydanie indywidualnej interpretacji prawa,</li>
                    <li>sporządzamy opinie, wnioski, odwołania i skargi z zakresu postępowania administracyjnego,</li>
                    <li>uczestniczymy w czynnościach kontrolnych,</li>
                    <li>pomagamy sporządzać umowy,</li>
                    <li>opracowujemy pisma procesowe,</li>
                    <li>doradzamy w zakresie finansów.</li>
                </ul>
            </li>
            <li id="i_p">
                <h2>Występujemy jako pełnomocnicy reprezentując naszych klientów przed:</h2>
                <ul>
                    <li>Sądami Administracyjnymi,</li>
                    <li>Izbami Skarbowymi,</li>
                    <li>Urzędami Skarbowymi,</li>
                    <li>Urzędami Kontroli Skarbowej,</li>
                    <li>Izbą Celną.</li>
                </ul>
            </li>
        </ul>
    </div>
</section>