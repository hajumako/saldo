<!DOCTYPE html>

<html lang="pl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0">

    <title>Saldo Chojnaccy | <?php echo get_title(); ?></title>
    <meta name="description" content="">
    <meta name="author" content="Quellio">

    <!--<meta name="robots" content="index,all,follow">-->
    <meta name="distribution" content="global">
    <meta name="author" content="Quellio">

    <meta property="og:site_name" content="Saldo Chojnaccy">
    <meta property="og:title" content="Saldo Chojnaccy">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta property="og:url" content="http://www.chojnaccy.com/">
    <meta property="og:type" content="website">
	

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="css/style.min.css">
    <link rel="stylesheet" href="css/subpages/<?php echo get_page_style(); ?>">

    <script src="js/jquery-3.1.1.slim.min.js" type="text/javascript"></script>
    <script src="js/main.min.js" type="text/javascript"></script>

    <!-- BEGIN callpage.io widget -->
    <script type="text/javascript">var __cp={"id":"jwS2bEtzI74wmZ3QbnG5933sxUrrz5yZt42cKQgTP5E","version":"1.1"};(function(window,document){var loader=function(){var cp=document.createElement('script');cp.type='text/javascript';cp.async=true;cp.src="++cdn-widget.callpage.io+build+js+callpage.js".replace(/[+]/g,'/').replace(/[=]/g,'.');var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(cp,s);};window.addEventListener?window.addEventListener("load",loader,false):window.attachEvent("onload",loader);if(window.callpage){alert('You could have only 1 CallPage code on your website!');}else{window.callpage=function(method){if(method=='__getQueue'){return this.methods;}
        else if(method){if(typeof window.callpage.execute==='function'){return window.callpage.execute.apply(this,arguments);}
        else{(this.methods=this.methods||[]).push({arguments:arguments});}}};window.callpage.__cp=__cp;window.callpage('api.button.autoshow');}})(window,document);</script>
    <!-- END callpage.io widget -->
	
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		ga('create', 'UA-87503531-1', 'auto');
		ga('send', 'pageview');
		
	</script>
</head>

<body>

<nav role="navigation" id="nav">
    <ul>
        <li id="back">
            <a href="#top">Powrót</a>
        </li>
        <li>
            <a href="o-nas">O nas</a>
        </li>
        <li>
            <a href="oferta">Oferta</a>
        </li>
        <li>
            <a href="porady">Porady</a>
        </li>
        <li>
            <a href="kontakt">Kontakt</a>
        </li>
        <li class="button-wrapper phone">
            <a class="button" href="tel:583440531">(58) 344 05 31</a>
            <p>Zadzwoń do nas</p>
        </li>
        <li class="button-wrapper email">
            <a class="button" href="mailto:biuro@chojnaccy.com">biuro@chojnaccy.com</a>
            <p>Napisz do nas</p>
        </li>
    </ul>
</nav>
<div id="canvas">
    <header class="cd-auto-hide-header">
        <div class="wrapper">
            <div class="upper cd-primary-nav">
                <div class="button-wrapper phone">
                    <a class="button" href="tel:583440531">(58) 344 05 31</a>
                    <p>Zadzwoń do nas</p>
                </div>
                <a id="logo" href="/">
                    <img src="img/logo.png" alt="Saldo Chojnaccy Logo"/>
                </a>
                <a id="top" href="#nav">
                    <div id="nav-icon">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <div class="button-wrapper email">
                    <a class="button" href="mailto:biuro@chojnaccy.com">biuro@chojnaccy.com</a>
                    <p>Napisz do nas</p>
                </div>
            </div>
            <div class="lower">
                <ul>
                    <li>
                        <a href="o-nas">O nas</a>
                    </li>
                    <li>
                        <a href="oferta">Oferta</a>
                    </li>
                    <li>
                        <a href="porady">Porady</a>
                    </li>
                    <li>
                        <a href="kontakt">Kontakt</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <main id="content">