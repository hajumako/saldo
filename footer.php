    <section class="motto">
        <div class="wrapper">
            <p>Jedyne czym musisz się zająć to&nbsp;rozwijanie&nbsp;własnej&nbsp;firmy.<br>Resztę załatwimy my.</p>
        </div>
    </section>

</main>
<footer class="footer">
    <div class="contact-data">
        <div class="wrapper">
            <h4>Kontakt</h4>
            <ul>
                <li class="phone">
                    <a href="tel:583440531">
                        <h5>Telefon</h5>
                        <span>(58) 344 05 31</span>
                    </a>
                </li>
                <li class="email">
                    <a href="mailto:biuro@chojnaccy.com">
                        <h5>Email</h5>
                        <span>biuro@chojnaccy.com</span>
                    </a>
                </li>
                <li class="nip">
                    <div>
                        <h5>NIP</h5>
                        <span>584 181 70 50</span>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="address">
        <div class="wrapper">
            <a href="https://goo.gl/maps/fGpTKHdXbMD2" target="_blank">
                <h6>ul. Chrzanowskiego 10/7, 80-278 Gdańsk</h6>
                <span class="button">Zobacz na mapie ></span>
            </a>
        </div>
    </div>
    <div class="links">
        <div class="wrapper">
            <div class="col col1">
                <img id="logo-w" src="img/logo-w.png" alt="Saldo Chojnaccy Logo"/>
                <ul class="social-media">
                    <li id="fb">
                        <a href="https://www.facebook.com/saldochojnaccy/" target="_blank">
                            <span>Facebook</span>
                        </a>
                    </li>
                    <li id="in">
                        <a href="https://www.linkedin.com/in/saldo-s-c-54611883" target="_blank">
                            <span>LinkedIn</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col col2">
                <h6>Krajowa&nbsp;Izba<br>Doradców&nbsp;Podatkowych</h6>
                <img src="img/logo-kidp-w.png" alt="KIDP Logo"/>
                <span>Wiesław Chojnacki nr wpisu: 03808</span>
            </div>
            <div class="col col3">
                <h6>Przydatne linki</h6>
                <ul>
                    <li>
                        <a href="http://www.mf.gov.pl/" target="_blank">
                            Strona Ministerstwa Finansów
                        </a>
                    </li>
                    <li>
                        <a href="http://e-inspektorat.zus.pl/" target="_blank">
                            Serwis obsługi klientów ZUS
                        </a>
                    </li>
                    <li>
                        <a href="http://www.kidp.pomorze.pl/" target="_blank">
                            Krajowa Izba Doradców Podatkowych
                        </a>
                    </li>
                    <li>
                        <a href="http://www.krd.pl/" target="_blank">
                            Krajowy Rejestr Długów
                        </a>
                    </li>
                    <li>
                        <a href="http://www.nbp.pl/" target="_blank">
                            Tabela kursów walut NBP
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="info">
        <div class="wrapper">
            <p id="copyright">Copyright &copy; Kancelaria Doradztwa Podatkowego Saldo Wiesław Chojnacki</p>
            <div>
                <span>Projekt i realizacja</span>
                <a href="http://quellio.com" target="_blank">
                    <img src="img/logo-q.png" alt="Quellio Logo"/>
                </a>
            </div>
        </div>
    </div>
</footer>
</div>
    <?php if(isset($_GET['page']) && $_GET['page'] == 'kontakt') : ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAm6sVN7Jzv-Lyxl-fEtTZpvrO9oq_Plt4&callback=initMap" async defer></script>
    <?php endif; ?>
</body>
</html>